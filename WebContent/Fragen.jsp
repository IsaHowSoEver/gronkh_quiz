<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="de.classes.quiz.test.query_questions"%>
<%@ page import="de.classes.quiz.test.query_question_options"%>
<%@ page import="de.classes.quiz.test.Counter"%>
<%@ page isThreadSafe="false" %>
<%


try {
	int iStatus = Integer.parseInt(request.getParameter("Status"));
	if (iStatus == 1) {
		Counter.iAntwortZähler = 1;
		Counter.iAuswertungZähler = 0;
		Counter.iFragenZähler = 1;	
	} 
} catch (Exception ex) {
	System.out.println(ex.getMessage());
}

	String sFrage;

	query_questions question = new query_questions();
	question.setFrage(Counter.iFragenZähler);
	sFrage = question.getFrage(); 

	String sAntwort1;
	query_question_options option1 = new query_question_options();
	option1.Verbindung();
	option1.Antwort1(Counter.iFragenZähler);
	sAntwort1 = option1.getAntwort1();

	String sAntwort2;
	query_question_options option2 = new query_question_options();
	option2.Verbindung();
	option2.Antwort2(Counter.iFragenZähler);
	sAntwort2 = option2.getAntwort2();

	String sAntwort3;
	query_question_options option3 = new query_question_options();
	option3.Verbindung();
	option3.Antwort3(Counter.iFragenZähler);
	sAntwort3 = option3.getAntwort3();

	String sAntwort4;
	query_question_options option4 = new query_question_options();
	option4.Verbindung();
	option4.Antwort4(Counter.iFragenZähler);
	sAntwort4 = option4.getAntwort4();

%>
<!DOCTYPE html>

<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=2">

<title>Das Gronkh-Quiz ☺</title>

<link href="bootstrap-3.3.5-dist/css/bootstrap.css" rel="stylesheet">

<script type="text/javascript">
function Regeln(){
	var String ='~Du bist hier bei einem Quiz zu Gronkh gelandet. \n ~Du hast 4 Anwortmöglichkeiten pro Frage. \n ~Unter der Fragen siehst du die Antwortmöglichkeiten, wo du eine auswählst. \n  ~Es wird dir nach dem Einloggen die richtige Lösung und ein Text mit interessanten Infos angezeigt. \n';
	alert(String);

}
</script>

</head>

<body>
	<form method="post" action="Antwort.jsp">
		<!-- Navigationsleiste -->

		<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

			<div class="container">

				<p class="navbar-brand">Gronkh-Quiz</p>

				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarResponsive" aria-controls="navbarResponsive"
					aria-expanded="false" aria-label="Toggle navigation">

					<span class="navbar-toggler-icon"></span>

				</button>

				<div class="collapse navbar-collapse" id="navbarResponsive">

					<ul class="navbar-nav ml-auto">

						<li class="nav-item"><a class="nav-link" onclick="Regeln()">Regeln
						</a></li>

					</ul>

				</div>

			</div>

		</nav>

		<div class="container">

			<!-- Jumbotron Header -->

			<header class="jumbotron my-4">

				<p class="lead">
					Frage
					<%
					out.println(Counter.iFragenZähler + ":");
					Counter.iFragenZähler++; 
				%>
					<%
						out.println(sFrage);
					%>
				</p>

			</header>

			<div class="row text-center">

				<!-- Antwort 1-->

				<div class="col-lg-3 col-md-6 mb-4">

					<div class="card h-100">

						<img class="card-img-top" src="Bilder/GronkhVogel.jpg">

						<div class="card-body">

							<h2 class="card-title">Antwort 1</h2>

							<strong class="card-text"> <%
 	out.println(sAntwort1);
 %>
							</strong>
						</div>

						<div class="card-footer">
							<button type="submit" class="btn btn-primary" name="Antwort1"
								value="<%out.print(sAntwort1);%>">Einloggen</button>
						</div>

					</div>

				</div>

				<!-- Antwort 2-->

				<div class="col-lg-3 col-md-6 mb-4">

					<div class="card h-100">

						<img class="card-img-top" src="Bilder/GronkhVogel.jpg">

						<div class="card-body">

							<h2 class="card-title">Antwort 2</h2>

							<strong class="card-text"> <%
 	out.println(sAntwort2);
 %>
							</strong>

						</div>

						<div class="card-footer">

							<button type="submit" class="btn btn-primary" name="Antwort2"
								value="<%out.print(sAntwort2);%>">Einloggen</button>

						</div>

					</div>

				</div>

				<!-- Antwort 3-->

				<div class="col-lg-3 col-md-6 mb-4">

					<div class="card h-100">

						<img class="card-img-top" src="Bilder/GronkhVogel.jpg">

						<div class="card-body">

							<h2 class="card-title">Antwort 3</h2>

							<strong class="card-text"> <%
 	out.println(sAntwort3);
 %>
							</strong>

						</div>

						<div class="card-footer">

							<button type="submit" class="btn btn-primary" name="Antwort3"
								value="<%out.print(sAntwort3);%>">Einloggen</button>

						</div>

					</div>

				</div>

				<!-- Antwort 4-->

				<div class="col-lg-3 col-md-6 mb-4">

					<div class="card h-100">

						<img class="card-img-top" src="Bilder/GronkhVogel.jpg">

						<div class="card-body">

							<h2 class="card-title">Antwort 4</h2>

							<strong class="card-text"> <%
 	out.println(sAntwort4);
 %>
							</strong>

						</div>

						<div class="card-footer">

							<button type="submit" class="btn btn-primary" name="Antwort4"
								value="<%out.print(sAntwort4);%>">Einloggen</button>

						</div>

					</div>

				</div>

			</div>

		</div>

		<!-- Footer -->

		<footer class="py-5 bg-dark fixed-bottom">
			<div class="container">
				<p class="m-0 text-center text-white">Copyright &copy;
					Gronkh-Quiz 2020</p>
			</div>

		</footer>
	</form>
</body>

</html>