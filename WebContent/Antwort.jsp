<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="de.classes.quiz.test.query_answer"%>
<%@ page import="de.classes.quiz.test.Counter"%>
<%@ page isThreadSafe="false" %>
<%
	String sAntwort;
	String sAntworttext;
	query_answer answer = new query_answer();
	answer.Verbindung();
	answer.setAntwort(Counter.iAntwortZähler);
	sAntwort = answer.getAnswer();
	sAntworttext = answer.getAnswertext();

	String sAntwort1 = request.getParameter("Antwort1");
	String sAntwort2 = request.getParameter("Antwort2");
	String sAntwort3 = request.getParameter("Antwort3");
	String sAntwort4 = request.getParameter("Antwort4");

	
	Counter.iAntwortZähler++;
%>

<!DOCTYPE html>

<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=2">

<title>Das Gronkh-Quiz ☺</title>

<link href="bootstrap-3.3.5-dist/css/bootstrap.css" rel="stylesheet">

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', function() {
	var counter = localStorage.getItem("counter");
	var bilder = ["Bilder/1.jpg", "Bilder/2.jpg", "Bilder/3.jpg",
			"Bilder/4.jpg", "Bilder/5.jpg", "Bilder/6.jpg", "Bilder/7.jpg",
			"Bilder/8.jpg", "Bilder/9.jpg", "Bilder/10.jpg",
			"Bilder/11.jpg", "Bilder/12.jpg", "Bilder/13.jpg",
			"Bilder/14.jpg", "Bilder/15.jpg", "Bilder/16.jpg",
			"Bilder/17.jpg", "Bilder/18.jpg", "Bilder/19.jpg",
			"Bilder/20.png"];
	if(counter === null){
		document.getElementById('pic').setAttribute('src', bilder[0]);
	}else{
		document.getElementById('pic').setAttribute('src', bilder[counter]);
	}
});
function PictureSwitch() {
		var counter = localStorage.getItem("counter");		
		if (counter === null) {
			counter = 0;
		}		
		counter++;		
		localStorage.setItem("counter", counter);
		
	}
function Regeln(){
	var String ='~Du bist hier bei einem Quiz zu Gronkh gelandet. \n ~Du hast 4 Anwortmöglichkeiten pro Frage. \n ~Unter der Fragen siehst du die Antwortmöglichkeiten, wo du eine auswählst. \n  ~Es wird dir nach dem Einloggen die richtige Lösung und ein Text mit interessanten Infos angezeigt. \n';
	alert(String);

}

</script>

</head>

<body>
	<!-- Navigationsleiste -->

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

		<div class="container">

			<p class="navbar-brand">Gronkh-Quiz</p>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">

				<span class="navbar-toggler-icon"></span>

			</button>

			<div class="collapse navbar-collapse" id="navbarResponsive">
				
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><a class="nav-link"
						href="Startseite.jsp?Status=1" onclick="Regeln()">Regeln </a></li>
					</ul>

			</div>


		</div>

	</nav>

	<div class="container">

		<!-- Jumbotron Header -->

		<header class="jumbotron my-4">

			<h1 class="display-3">Hier ist deine Antwort:</h1>

			<p class="lead">

				<strong> <%
 	if (sAntwort1 != null) {
 		if (sAntwort1.equals(sAntwort)) {
 			out.println("Richtig!");
 			Counter.iAuswertungZähler++;
 		} else {
 			out.println("Falsch!");
 		}
 	} else if (sAntwort2 != null) {
 		if (sAntwort2.equals(sAntwort)) {
 			out.println("Richtig!");
 			Counter.iAuswertungZähler++;
 		} else {
 			out.println("Falsch!");
 		}
 	} else if (sAntwort3 != null) {
 		if (sAntwort3.equals(sAntwort)) {
 			out.println("Richtig!");
 			Counter.iAuswertungZähler++;
 		} else {
 			out.println("Falsch!");
 		}
 	} else if (sAntwort4 != null) {
 		if (sAntwort4.equals(sAntwort)) {
 			out.println("Richtig!");
 			Counter.iAuswertungZähler++;
 		} else {
 			out.println("Falsch!");
 		}
 	}
 %>
				</strong>
				<%
					out.print("Die Antwort ist: ");
				%><strong>
					<%
						out.print(sAntwort + "☺");
					%>
				</strong> <br>
				<%
					out.println(sAntworttext);
				%>
			</p>

		</header>

		<img id="pic" class="card-img-top border border-dark"
			src="Bilder/GronkhVogel.jpg">

		<header class="jumbotron my-4 w-100">
			<a href="Fragen.jsp"><button type="button" class="btn center-block"  onclick="PictureSwitch()">
					<p class="display-5">
						<strong>Weiter</strong>
					</p>
				</button></a>
		</header>
		<header class="jumbotron my-4 w-100">
			<a href="Auswertung.jsp"><button class="btn center-block">
					<p class="display-5">
						<strong>Auswertung</strong>
					</p>
				</button></a>
		</header>
	</div>
	<br>
	<br>
	<br>
	<!-- Footer -->

	<footer class="py-5 bg-dark fixed-bottom">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy;
				Gronkh-Quiz 2020</p>
		</div>
	</footer>

</body>

</html>