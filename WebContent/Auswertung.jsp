<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="de.classes.quiz.test.Counter"%>
<%@ page isThreadSafe="false" %>
<!DOCTYPE html>

<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=2">

<title>Das Gronkh-Quiz ☺</title>

<link href="bootstrap-3.3.5-dist/css/bootstrap.css" rel="stylesheet">

<script type="text/javascript">
	localStorage.clear();

function Regeln(){
	var String ='~Du bist hier bei einem Quiz zu Gronkh gelandet. \n ~Du hast 4 Anwortmöglichkeiten pro Frage. \n ~Unter der Fragen siehst du die Antwortmöglichkeiten, wo du eine auswählst. \n  ~Es wird dir nach dem Einloggen die richtige Lösung und ein Text mit interessanten Infos angezeigt. \n';
	alert(String);

}
</script>

</head>

<body>		
	<form method="post" action="Startseite.jsp?Status=1"> <!-- Anstatt Fragen.jsp auf Startseite.jsp geändert -->
	<!-- Navigationsleiste -->

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

		<div class="container">

			<p class="navbar-brand">Gronkh-Quiz</p>

			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">

				<span class="navbar-toggler-icon"></span>

			</button>

			<div class="collapse navbar-collapse" id="navbarResponsive">

				<ul class="navbar-nav ml-auto">
					
					<li class="nav-item"><a class="nav-link" onclick="Regeln()">Regeln </a></li>

					</li>

				</ul>

			</div>

		</div>

	</nav>

	<div class="container">

		<!-- Jumbotron Header -->

		<header class="jumbotron my-4">

			<h1 class="display-3">Hier ist deine Antwort:</h1>

			<p class="lead">
				<%
					int Auswertung = Counter.iAuswertungZähler;
					if(Auswertung == 0){
						out.println("Aua... Ganz dringend Streams & Let's Plays schauen ☺ Leider keine einzige Frage richtig.");
					}
					else if (Auswertung <= 5) {
						out.println("Das muss wohl noch einiges nachgeholt werden... Gerade mal ");
						%>
						<strong>
						<% 
						out.println(Auswertung);
						%>
						</strong>
						<%
						out.println(" richtige Antworten. Da muss wohl noch einiges an Streams & Let's Plays geschaut werden. ☺");
					} else if (Auswertung <= 10) {
						if (Auswertung > 5) {
							out.println("Schon besser aber immer noch zu wenig... Gerade mal ");
							%>
							<strong>
							<% 
							out.println(Auswertung);
							%>
							</strong>
							<%
							out.println(" richtige Antworten.  Für einen richtigen 'Kennner' muss noch viel an Streams & Let's Plays geschaut werden. ☺");
						}
					} else if (Auswertung <= 15) {
						if (Auswertung > 10) {
							out.println("Geht schon in die richtige Richtung... ");
							%>
							<strong>
							<% 
							out.println(Auswertung);
							%>
							</strong>
							<%
							out.println(" richtige Antworten hast du erziehlt. Empfehlenswert ist trotzdem sich noch ein paar Streams & Let's Plays anzuschauen. ☺");
						}
					} else if (Auswertung < 20) {
						if (Auswertung > 15) {
							out.println("Ein schönes Ergebnis... ");
							%>
							<strong>
							<% 
							out.println(Auswertung);
							%>
							</strong>
							<%
							out.println(" richtige Antworten. Vielleicht noch ein wenig mehr Streams & Let's Plays schauen und dann ist die volle Punktzahl auch drin. ☺");
						}
					}else if(Auswertung == 20){
						out.println("Das nenne ich einen Kenner. Ein richtiger Gronkh-Experte ☺ 20 richtige Antworten. Seine Streams & Videos schauen schadt ja trotzdem nicht ☺");
					}
				%>
			</p>

		</header>
		<img class="card-img-top border border-dark"
			src="Bilder/GronkhVogel.jpg">
		<header class="jumbotron my-4 w-100">
			<button  type="submit" class="btn center-block" value="20"> 
			<p class="display-5">
						<strong>Startseite</strong>
					</p>
		</button>
		</header>
	</div>
	<br>
	<br>
	<br>
	<!-- Footer -->

	<footer class="py-5 bg-dark fixed-bottom">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy;
				Gronkh-Quiz 2020</p>
		</div>
	</footer>

</form>
</body>

</html>