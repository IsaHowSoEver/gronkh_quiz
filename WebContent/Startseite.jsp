<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="de.classes.quiz.test.Counter"%>
<%
try {
	int iStatus = Integer.parseInt(request.getParameter("Status"));
	if (iStatus == 1) {
		Counter.iAntwortZähler = 1;
		Counter.iAuswertungZähler = 0;
		Counter.iFragenZähler = 1;	
	} 
} catch (Exception ex) {
	System.out.println(ex.getMessage());
}
%>
<!DOCTYPE html>
<html>
<head> 
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=2">
<link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.css">
<style>
body {
	background-image: url("Bilder/GronkhVogelTransparent.jpg");
}
</style>
<title>Gronkh-Quiz Startseite</title>

</head>
<body>

	<div class="modal-content text-center container">
		<header class="jumbotron my-4">
			<h1 class="display-3">Willkommen beim Quiz!</h1> 
			<strong class="display-5">Hoffe du nimmst ein paar interessante Infos mit. ☺</strong> 

			
		</header>
	</div>

	<div class="modal-content text-center container center-block">
		<a href="Fragen.jsp">
			<button class="btn btn-default btn-xs modal-content">
				<h4>
					<strong>--> Hier startest du das Spiel <--</strong>
				</h4>
				<p>
				~Du bist hier bei einem Quiz zu Gronkh gelandet. <br>
				~Du hast 4 Anwortmöglichkeiten pro Frage. <br>
				~Unter der Fragen siehst du die Antwortmöglichkeiten, wo du eine auswählst. <br>
				~Es wird dir nach dem Einloggen die richtige Lösung und ein Text mit interessanten Infos angezeigt. <br>
				</p>
			</button>
		</a>
	</div>
</body>
