-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 06. Mai 2020 um 13:34
-- Server-Version: 10.3.16-MariaDB
-- PHP-Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `db_gronkhquiz`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tb_antwort`
--

CREATE TABLE `tb_antwort` (
  `ID` int(3) NOT NULL,
  `ID_Frage` int(3) NOT NULL,
  `Antworten` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Antworttext` varchar(10000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `tb_antwort`
--

INSERT INTO `tb_antwort` (`ID`, `ID_Frage`, `Antworten`, `Antworttext`) VALUES
(1, 1, 'Erik Range', 'Sein richtiger Name ist \"Erik Range\". \r\n				Anfangs schertze er, dass Gronkh von seinem richtigen Namen \r\n				Gregor onkh abgeleitet wäre.\r\n				Das stimmte aber natürlich nicht.☺'),
(2, 2, '10.April.1977', 'Geboren wurde er am 10.April.1977. \r\n				Fragt man, ihn behauptet er, ironischer Weise natürlich, er ist fast 12.☺'),
(3, 3, 'Let\'s Plays', 'Hauptsächlich ist er für seine Let\'s Plays bekannt. \r\n				Mit Let\'s Plays ist gemeint, dass er Spiele spielt und kommentiert\r\n				Das macht er mit seiner humorvollen und symaptischen Art.☺'),
(4, 4, 'Fachinformatiker', 'Tatsächlich hat er schon eine erfolgreich abgeschlossene Ausbildung als Fachinformatiker hinter sich.\r\n				Kleiner Fun-Fact am Rande: Seinen Zivildienst absolvierte er in einem Kindergarten.☺'),
(5, 5, 'Sprechrollen, Musik & Streams', 'Neben den Let\'s Plays, hat er noch viele weitere Tätigkeiten\r\n				U.A. hatte er schon Sprechrollen in Filmen & Spielen, \r\n				er hat schon ein Lied raus gebracht & \r\n				er veranstaltet regelmäßige Streams auf Twich.\r\n				Wen es interessiert: \r\n				Freitags ab 18Uhr. Dann redet er meistens aber erstmal 2 Stunden, bevor er anfängt zu spielen.☺'),
(6, 6, '2010', 'Seinen YouTube Channel hat er bereits seit 2006. Let\'s Plays von ihm sind seit 2010 zu finden.	\r\n				In seiner Kanalinfo ist folgender Satz zu finden:\r\n				\"Tägliche Folgen nonstop seit dem 01.04.2010!\"\r\n				Wobei das tatsächlich auch nicht die Wahrheit ist. Es gab schonmal einen Tag, wo keine Folge hochgeladen wurde.☺'),
(7, 7, 'Russisch, Deutsch', 'Seinen Vater ist deutscher Abstammung & seine Mutter russischer Abstammung .	\r\n				Demnach hat er die Nationalitäten Russisch & Deutsch.\r\n				Wer seine Englische-Aussprache kennt wird wissen, dass eine britische/amerikanische Nationalität vorne und hinten nicht passen könnte.☺'),
(8, 8, 'Play Massive', '2007 gründete er die Play Massive, welche er bis zum 01.Januar.2018 mit leitete.\r\n				Die Play Massive beschäfitgt sich mit allem Rund um Spiele. Von bekannten Spielen bis hin zu Newcomern.☺'),
(9, 9, '1', 'Er hat einen älteren Bruder. Soviel gibt er Preis.\r\n				Wie er heißt, aussieht oder was er macht ist aber nicht bekannt.☺'),
(10, 10, 'Valentin Rahmel', 'Geleitet hat er das Unternehmen Play Massive mit seinem damals noch sehr guten Freund Valentin Rahmel aka Sarazar.☺\r\n				Wieso sie sich sowohl geschäftlich als auch Freundschaftlich getrennt hatten, ist bis dato immer noch nicht bekannt\r\n				Sarazar hat sich mehr in die Musik & Reise-Richtung begeben und Gronk ist weiter bei seinen Let\'s Play geblieben.\r\n				Laut Gerüchten soll es wohl nicht mehr zusammen gepasst haben. ☺'),
(11, 11, 'Tatjana Werth', 'Seine Lebensgefährtin heißt Tatjana Werth aka Pandorya.\r\n				Ein Paar sind sie seit Ende 2012, wie er in einem Livestream 2016 berichtete.\r\n				Gronkh hatte sie damals angeschrieben, woraufhin sie lange Zeit geschrieben haben und sich Ende 2012 getroffen haben und ein Paar wurden.☺'),
(12, 12, 'Gods of Gaming', 'Im Juni 2018 gründete er sein neues Unternehmen Gods of Gaming.\r\n				\"Wir tun Dinge, dir wir gut finden, um Menschen zu unterhalten, die wir mögen.\" ist das neue Motto des Unternehmens.\r\n				Ansässig ist Gods of Gaming in den Kranhäusern in Köln.☺'),
(13, 13, 'Nicht mein Tag', 'Er spielt in dem Film Nicht mein Tag, von Peter Thorwarth mit.\r\n				Der Film ist aus dem Jahr 2014.\r\n				Er spielte noch in weiteren Filmen mit, wie z.B. Planet der Affen oder LEGO Batman Movie.\r\n				Darüber hinaus hatte er zahlreiche Sprechrollen in Spielen, wie Deponia, Witcher 3, Just Cause 3 usw.\r\n				Ein echtes Multitalent.☺ '),
(14, 14, 'Horror-Spiele', 'Ja tatsächlich sind es leider die Horror Spiele die bei ihm etwas zu kurz kommen.\r\n				Er gibt an, dass es einfach nicht so sein Genre ist. Er spielt lieber andere Richtungen.\r\n				Aber insgeheim wissen wir alle, dass er einfach Angst hat.☺'),
(15, 15, 'HWSQ', 'Die Gaming-Gruppe, die es bis Sommer 2019 aktiv war, hieß HWSQ.\r\n				Sie spielten alle möglichen Multiplayer Spiele, die sie in die Finger bekamen.\r\n				Aufgrund eines Vorfalls mit dem Mitglied Kevin Dorissen aka HerrCurrywurst, trennte sich die Gruppe leider.☺'),
(16, 16, '2015', 'Ausgezeichnet wurde er 2015.\r\n				Die Academy des Deutschen Webvideopreises würdigte Gronkh damit als einen „der ersten Let’s Player in Deutschland überhaupt“, \r\n				der nun der „erfolgreichste YouTuber in Deutschland“ sei.(Bis Dato 2015)\r\n				Auch 2012 wurde er schon mit Preisen ausgezeichnet. U.A. für das Video GTA 5 – Real Life Grand Theft Auto.\r\n				2013 erhielt er den Goldenen Play-Button für mehr als 1Mio Abonnenten auf seinem Kanal.\r\n				Trotz dessen hätte er noch viel mehr Preise verdient.☺'),
(17, 17, '5.000 Volt', 'Das Lied trug den Namen 5.000 Volt. Veröffentlicht wurde sie am 06.09.2013.\r\n				Das Lied entstand aus einem Insider der damaligen Show LPT.\r\n				Er selber sieht das Ganze auch nur als einen Spaß.\r\n				Gronhk hatte niemals vor damit Erfolg zu haben oder ernst dahinter zu stehen.☺'),
(18, 18, 'Braunschweig', 'Zusammen mit seinem Bruder wuchs er in Braunschweig auf.\r\n				Genauer gesagt in der Weststadt. Bedeutet im Plattenbau.\r\n				Alles, was er heute erreicht hat, hat er sich Mühsam erarbeitet.☺'),
(19, 19, 'Friendly Fire', 'Der Charity-Livestream Firendly Fire sorgt immer für gute Stimmung und Einnahmen.\r\n				Sei 2015 findet er jährlich am Ende des Jahres statt.\r\n				Im Stream findet man alle möglichen Aktionen. \r\n				Diese reichen vom spielen aller möglichen Spiele oder lustige Aktionen, \r\n				wie einem der Teilnehmer die Haare abrasieren.☺'),
(20, 20, 'Gronkh war in der Hip Hop Szene aktiv', 'Das folgende Bild zeigt ihn ihm Alter von 24Jahren.\r\n				Was er nie erzählt ist, dass er in der Hip Hop Szene aktiv war. 	\r\n				Genauer gesagt als Rapper unter dem Namen \"Das e\".\r\n				2002 brachte er z.B. mit den Rappern Cappuccino und Krizz den Song \"La Familia\" raus.\r\n				Noch ein kleiner Fun-Fact. Der Song wurde ursprünglich für GTA 4 aufgenommen.☺');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tb_antwortmöglichkeiten`
--

CREATE TABLE `tb_antwortmöglichkeiten` (
  `ID` int(3) NOT NULL,
  `ID_Frage` int(3) NOT NULL,
  `Antwort_1` varchar(50) NOT NULL,
  `Antwort_2` varchar(50) NOT NULL,
  `Antwort_3` varchar(50) NOT NULL,
  `Antwort_4` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `tb_antwortmöglichkeiten`
--

INSERT INTO `tb_antwortmöglichkeiten` (`ID`, `ID_Frage`, `Antwort_1`, `Antwort_2`, `Antwort_3`, `Antwort_4`) VALUES
(1, 1, 'Gregor Onkh', 'Gregor Range', 'Erik Range', 'Erik Onkh'),
(2, 2, '10.April.1977', '11.Februar.1976', '10.April.1987', '11.Februar.1979'),
(3, 3, 'Let\'s Plays', 'Influencer sein', 'Schauspiel', 'Geschäftsführer sein'),
(4, 4, 'Elektroniker', 'Fachinformatiker', 'Maurer', 'Mediengestalter'),
(5, 5, 'Musik, Fotoshootings & Beauty Videos', 'Schauspiel, Sprechrollen & Tanzauftritte', 'Sprechrollen, Musik & Streams', 'Vlogs, Streams & Angestellter Fachinformatiker'),
(6, 6, '2009', '2012', '2011', '2010'),
(7, 7, 'Russisch, Deutsch', 'Amerikanisch, Deutsch', 'Russisch, Amerikanisch', 'Britisch, Deutsch'),
(8, 8, 'Range-IT', 'Play Massive', 'Game Massive', 'Range Massive'),
(9, 9, '1', '2', '8', '4'),
(10, 10, 'Valetin Müller', 'Matthias Müller', 'Valentin Rahmel', 'Matthias Rahmel'),
(11, 11, 'Melanie Wende', 'Tatjana Wende', 'Melanie Werth', 'Tatjana Werth'),
(12, 12, 'Gods of Play', 'Gaming of Gods', 'Play of Gods', 'Gods of Gaming'),
(13, 13, 'Nicht dein Tag', 'Nicht mein Tag', 'Nicht ihr Tag', 'Nicht sein Tag'),
(14, 14, 'Action-Spiele', 'Survival-Spiele', 'Horror-Spiele', 'Rollenspiele'),
(15, 15, 'GPTH', 'HWGQ', 'GPSQ', 'HWSQ'),
(16, 16, '2014', '2010', '2015', '2012'),
(17, 17, '5.000 Volt', '5.000 Games', '5.000 Stunden', '5.000 Watt'),
(18, 18, 'Darmstadt', 'Kleve', 'Frankfurt', 'Braunschweig'),
(19, 19, 'Friendly Gaming', 'Fire Gaming', 'Friendly Fire', 'Friendly Collecting'),
(20, 20, 'Gronkh war in der Hip Hop Szene aktiv', 'Gronkh hat Ballett getanzt', 'Gronkh hatte einen Schulverweis', 'Gronkh hat einen Sohn');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tb_fragen`
--

CREATE TABLE `tb_fragen` (
  `ID` int(3) NOT NULL,
  `Fragen` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `tb_fragen`
--

INSERT INTO `tb_fragen` (`ID`, `Fragen`) VALUES
(1, 'Wie lautet Gronkh sein richtiger Name?'),
(2, 'Wann feiert er Geburtstag?'),
(3, 'Für was ist Gronkh hauptsächlich bekannt?'),
(4, 'Welche Ausbildung hat er gemacht?'),
(5, 'Welche weiteren Tätigkeiten übt er aus?'),
(6, 'Seit wann sind Let\'s Plays von ihm zu finden?'),
(7, 'Welche Nationalitäten besitzt Gronkh?'),
(8, 'Von welchem Unternehmen war er bis 2018 Geschäftsführer?'),
(9, 'Wie viele Geschwister hat er?'),
(10, 'Mit wem leitete er das Unternehmen?'),
(11, 'Wie heißt seine Lebensgefährtin?'),
(12, 'Wie heißt sein neues Unternehmen?'),
(13, 'In welchem Film hat er u.A. mitgespielt?'),
(14, 'Welches Genre bedient er bei seinen Let\'s Plays am wenigsten?'),
(15, 'Wie hieß die Gaming-Gurppe um Pandorya, Tobinator, HerrCurrywurst & Gronkh?'),
(16, 'Wann wurde er mit dem Webvideopreis \"Ehrenpreis national\" ausgezeichnet?'),
(17, 'Wie hieß der Song, den Gronkh zusammen mit Sarazar veröffentlichte?'),
(18, 'In welcher Stadt wuchs Gronkh auf?'),
(19, 'Bei welchem großen Stream sammeln Gronkh & andere YouTuber Geld für gemeinnützige Zwecke?'),
(20, 'Welchen Fun-Fact weiß kaum jemand über Gronkh?');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tb_antwort`
--
ALTER TABLE `tb_antwort`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID_Frage` (`ID_Frage`);

--
-- Indizes für die Tabelle `tb_antwortmöglichkeiten`
--
ALTER TABLE `tb_antwortmöglichkeiten`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID_Frage` (`ID_Frage`);

--
-- Indizes für die Tabelle `tb_fragen`
--
ALTER TABLE `tb_fragen`
  ADD PRIMARY KEY (`ID`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `tb_fragen`
--
ALTER TABLE `tb_fragen`
  ADD CONSTRAINT `tb_fragen_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `tb_antwortmöglichkeiten` (`ID_Frage`),
  ADD CONSTRAINT `tb_fragen_ibfk_2` FOREIGN KEY (`ID`) REFERENCES `tb_antwort` (`ID_Frage`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
