package de.classes.quiz.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.classes.quiz.test.Counter;
import de.classes.quiz.test.db_verbindung;

public class query_questions {

	private Connection conn = null;
	private ResultSet rs = null;
	private PreparedStatement ps = null;
	private String sQuery = "SELECT Fragen FROM tb_fragen WHERE ID = ? ";
	
	private String Frage;
	private int iID;
	
	public void setFrage(int iID1) throws SQLException, ClassNotFoundException {
		db_verbindung Verbindung = new db_verbindung(
				"jdbc:mysql://localhost:3306/db_gronkhquiz?useTimezone=true&serverTimezone=UTC&useUnicode=yes&characterEncoding=utf8", "root", "");

		conn = Verbindung.getConnection();
		iID = Counter.iFragenZähler;
		try {
			ps = conn.prepareStatement(sQuery);
			ps.setInt(1, iID);
			rs = ps.executeQuery();

			if (rs.next()) {
				Frage = rs.getString("fragen");
				System.out.println(Frage);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		finally {
			conn.close();
			ps.close();
			rs.close();

		}
	}
	public String getFrage() {
		return Frage;
	}
}
