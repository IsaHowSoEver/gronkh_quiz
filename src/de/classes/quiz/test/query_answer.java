package de.classes.quiz.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.classes.quiz.test.Counter;
import de.classes.quiz.test.db_verbindung;

import java.lang.String;

public class query_answer {
	
	private Connection conn = null;
	private ResultSet rs = null;
	private PreparedStatement ps = null;
	private String sQuery = "SELECT Antworten, Antworttext FROM tb_antwort WHERE ID_Frage = ? ";
	private String sAntwort;
	private String sAntworttext;
	private int iID;

	public void Verbindung() throws SQLException, ClassNotFoundException {
		db_verbindung oVerbindung = new db_verbindung(
				"jdbc:mysql://localhost:3306/db_gronkhquiz?useTimezone=true&serverTimezone=UTC&useUnicode=yes&characterEncoding=utf8", "root", "");

		conn = oVerbindung.getConnection();
	}

	public void setAntwort(int iID1) throws SQLException, ClassNotFoundException {
		iID = Counter.iAntwortZähler;
		try {
			ps = conn.prepareStatement(sQuery);
			ps.setInt(1, iID);
			rs = ps.executeQuery();

			if (rs.next()) {
				sAntwort = rs.getString("Antworten");
				sAntworttext = rs.getString("Antworttext");
				System.out.println(sAntwort);
				System.out.println(sAntworttext);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		finally {
			conn.close();
			ps.close();
			rs.close();
		}
	}

	public String getAnswer() {
		return sAntwort;
	}
	public String getAnswertext() {
		return sAntworttext;
	}
}
