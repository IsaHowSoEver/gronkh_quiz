package de.classes.quiz.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.classes.quiz.test.Counter;
import de.classes.quiz.test.db_verbindung;

public class query_question_options {

	private Connection conn = null;
	private ResultSet rs = null;
	private PreparedStatement ps = null;
	private String sQuery1 = "SELECT Antwort_1 FROM tb_antwortmöglichkeiten WHERE ID_Frage = ? ";
	private String sQuery2 = "SELECT Antwort_2 FROM tb_antwortmöglichkeiten WHERE ID_Frage = ? ";
	private String sQuery3 = "SELECT Antwort_3 FROM tb_antwortmöglichkeiten WHERE ID_Frage = ? ";
	private String sQuery4 = "SELECT Antwort_4 FROM tb_antwortmöglichkeiten WHERE ID_Frage = ? ";

	private String Antwort1;
	private String Antwort2;
	private String Antwort3;
	private String Antwort4;	

	private int iID;

	public void Verbindung() throws SQLException, ClassNotFoundException {
		db_verbindung Verbindung = new db_verbindung(
				"jdbc:mysql://localhost:3306/db_gronkhquiz?useTimezone=true&serverTimezone=UTC&useUnicode=yes&characterEncoding=utf8", "root", "");
		conn = Verbindung.getConnection();
	}

	public void Antwort1(int iID1) throws SQLException, ClassNotFoundException {
		iID = Counter.iFragenZähler;
		
		try {
			ps = conn.prepareStatement(sQuery1);
			ps.setInt(1, iID);
			rs = ps.executeQuery();
			if (rs.next()) {
				Antwort1 = rs.getString("Antwort_1");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		finally {
			conn.close();
			ps.close();
			rs.close();
		}

	}

	public void Antwort2(int iID1) throws SQLException, ClassNotFoundException {
		iID = iID1;
		
		try {
			ps = conn.prepareStatement(sQuery2);
			ps.setInt(1, iID);
			rs = ps.executeQuery();
			if (rs.next()) {
				Antwort2 = rs.getString("Antwort_2");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		finally {
			conn.close();
			ps.close();
			rs.close();
		}
	}

	public void Antwort3(int iID1) throws SQLException, ClassNotFoundException {
		iID = iID1;;
		
		try {
			ps = conn.prepareStatement(sQuery3);
			ps.setInt(1, iID);
			rs = ps.executeQuery();
			if (rs.next()) {
				Antwort3 = rs.getString("Antwort_3");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		finally {
			conn.close();
			ps.close();
			rs.close();
		}
	}

	public void Antwort4(int iID1) throws SQLException, ClassNotFoundException {
		iID = iID1;
		
		try {
			ps = conn.prepareStatement(sQuery4);
			ps.setInt(1, iID);
			rs = ps.executeQuery();
			if (rs.next()) {
				Antwort4 = rs.getString("Antwort_4");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		finally {
			conn.close();
			ps.close();
			rs.close();
		}
	}

	public String getAntwort1() {
		return Antwort1;
	}

	public String getAntwort2() {
		return Antwort2;
	}

	public String getAntwort3() {
		return Antwort3;
	}

	public String getAntwort4() {
		return Antwort4;
	}
}