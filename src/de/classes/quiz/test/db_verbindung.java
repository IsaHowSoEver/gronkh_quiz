package de.classes.quiz.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class db_verbindung {
	private String sUrl;
	private String sBenutzer;
	private String sPasswort;
	
	public db_verbindung(String Url, String Benutzer, String Passwort) throws ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		this.sUrl = Url;
		this.sBenutzer = Benutzer; 
		this.sPasswort = Passwort;
	}		
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(sUrl ,sBenutzer, sPasswort);
	}
}

